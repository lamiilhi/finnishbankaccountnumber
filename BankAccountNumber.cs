﻿using System;
using System.Text;

namespace FinnishBankAccountNumber
{
    /// <summary>
    /// A Class for a bank account number.
    /// Contains a method for converting the short bank account number to the long format while constructing the instance.
    /// </summary>
    class BankAccountNumber
    {
        public const int NUMBER_MAXLENGTH = 14;    // Length of the number format
        public String bank;


        public String sAccountNumber;           // s, as in short
        public String lAccountNumber;           // l, as in long, the machine readable markup


        /// <summary>
        /// A Constructor for the bank account number
        /// </summary>
        /// <param name="accountnumber">The bank account number in its' "old" format.</param>
        public BankAccountNumber(String sAccountnumber)
        {
            sAccountNumber = sAccountnumber;
            bank = DetermineBank(sAccountnumber);
            lAccountNumber = ConvertAccountNumber(bank, sAccountnumber);


        }



        /// <summary>
        /// A method for converting the account number to the long, machine readable, format.
        /// </summary>
        /// <param name="bank"></param>
        /// <param name="accountnumber"></param>
        /// <returns>The long account number format</returns>
        private String ConvertAccountNumber(String bank, string accountnumber)
        {
            int[] helpArray = new int[13];          //Array for filling with the numbers from the bankaccountnumber string
            int[] luhnArray = { 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 };    //Array for the Luhn Modulo
            int[] sumArray = new int[13];           //Array for the sum of the Luhn modulo
            int checkSum = 0;
            int nearestTen;
            int checkDigit;

            String longAccountnumber = "";                                          // The converted bank number with the zeroes added and the luhn modulo checked

            if (bank == "Nordea"                                            //Determine which bank
              ||bank == "Handelsbanken"
              ||bank == "Skandinaviska Enskilda Banken"
              ||bank == "Danske Bank"
              ||bank == "Tapiola Pankki"
              ||bank == "DnB NOR Bank ASA"
              ||bank == "Swedbank"
              ||bank == "S-Pankki"
              ||bank == "Ålandsbanken"
              ||bank == "Sampo Pankki")
            {
                StringBuilder sbAccountnumber = new StringBuilder(accountnumber);

                sbAccountnumber.Replace("-", "0000000");    
                while (sbAccountnumber.Length > NUMBER_MAXLENGTH)
                {
                    int i;
                    i = sbAccountnumber.ToString().IndexOf('0');
                    sbAccountnumber.Remove(i, 1);
                }
                longAccountnumber = sbAccountnumber.ToString();
                
            }


            if (bank == "Sp, Pop & Aktia"
              ||bank == "Op, OKO & Okopankki")
            {
                StringBuilder sbAccountnumber = new StringBuilder(accountnumber);
                sbAccountnumber.Replace("-", "");
                sbAccountnumber.Insert(7, "0000000", 1);
                while (sbAccountnumber.Length > NUMBER_MAXLENGTH)
                {
                    int i;
                    i = sbAccountnumber.ToString().IndexOf('0');
                    sbAccountnumber.Remove(i, 1);
                }
                longAccountnumber = sbAccountnumber.ToString();
            }



            for (int i = 0; i < helpArray.Length; i++)    //Filling the arrays and calculating the checksum
            {
                helpArray[i] = (longAccountnumber[i] - '0');
            }

            for (int i = (helpArray.Length -1); i>-1; i--)
            {
                sumArray[i] = helpArray[i] * luhnArray[i];
            }

            
            for (int i = 0; i < sumArray.Length; i++)           
            {
                int[] csArray = new int[20];                 //allocating enough room
                if (sumArray[i] > 9 )
                {
                    String helpS = sumArray[i].ToString();
                    checkSum += Convert.ToInt32(new String(helpS[0], 1));                   //Capturing the single digits to the sum instead of the whole number.
                    checkSum += Convert.ToInt32(new String(helpS[1], 1));                  //Assuming that they cant be 3 digits long.
                }

                else checkSum += sumArray[i]; 
            }

            nearestTen = ((int)Math.Round(checkSum / 10.0)) * 10;
            checkDigit = nearestTen - checkSum;
            if (checkDigit != Convert.ToInt32(new string(accountnumber[accountnumber.Length - 1], 1)))
            {
                return "Error, checksum wrong.";    
            }                                       
            else return longAccountnumber;          //  Returning the long format of the bank account number
        }



        
        /// <summary>
        /// A method for selecting the correct bank for the account.
        /// </summary>
        /// <param name="sAccountNumber"></param>
        /// <returns></returns>
        private String DetermineBank(String sAccountNumber)
        {
            int compareHelp = Int16.Parse(sAccountNumber[0].ToString() + sAccountNumber[1].ToString());

            switch (compareHelp)
            {
                case int n when (n < 30): return "Nordea";
                    break;
                case 31: return "Handelsbanken";
                    break;
                case 33: return "Skandinaviska Enskilda Banken";
                    break;
                case 34: return "Danske Bank";
                    break;
                case 36: return "Tapiola Pankki";
                    break;
                case 37: return "DnB NOR Bank ASA";
                    break;
                case 38: return "Swedbank";
                    break;
                case 39: return "S-pankki";
                case int n when (n > 39 && n < 50): return "Sp, Pop & Aktia";
                    break;
                case int n when (n > 49 && n < 60): return "Op, OKO & Okopankki";
                    break;
                case int n when (n > 59 && n < 80): return "Ålandsbanken";
                    break;
                case int n when (n > 89 && n < 90): return "Sampo Pankki";
                    break;
                default: return "Unrecognized bank";
                    break;
            }
            
        }
    }
}