﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace FinnishBankAccountNumber
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BankAccountNumber ban;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void longNumber_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            String s = shortNumber.Text;
            Boolean errtest = true;                                 
            String err = "Error! Use format: '012345-1537'";

            if (s.Length > BankAccountNumber.NUMBER_MAXLENGTH  )    // Checking the length 
            {
                longNumber.Text = err;                                                  //If the entered number is wrong format, an error message is displayed in the second field.
                errtest = false;
            }


            for (int i = 0; i < s.Length; i++)
            {
                while (i < 6 && i > 6)
                {
                    if (Char.IsNumber(s[i]) == false )
                    {
                        longNumber.Text = err;                  //Displays an error message if letters are inserted where numbers should be.
                        errtest = false;                        //Setting the error indicator to false
                    }
                }
            }


            if (errtest == true)
            {
                ban = new BankAccountNumber(shortNumber.Text);  // If the errortest is passed, the value is passed to the constructor,
                longNumber.Text = ban.lAccountNumber;           // the number is converted to long format and finally displayed in the 2nd field.
                bank.Text = ban.bank;
            }
        }
    }
}
